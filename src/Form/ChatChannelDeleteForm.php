<?php

namespace Drupal\chat_channels\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Chat channel entities.
 *
 * @ingroup chat_channels
 */
class ChatChannelDeleteForm extends ContentEntityDeleteForm {


}
