<?php

namespace Drupal\chat_channels\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Chat channel type entities.
 */
interface ChatChannelTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
