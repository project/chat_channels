<?php

namespace Drupal\chat_channels\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Chat channel message entities.
 *
 * @ingroup chat_channels
 */
class ChatChannelMessageDeleteForm extends ContentEntityDeleteForm {


}
