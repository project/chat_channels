<?php

namespace Drupal\chat_channels\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Chat channel member entities.
 *
 * @ingroup chat_channels
 */
class ChatChannelMemberDeleteForm extends ContentEntityDeleteForm {


}
